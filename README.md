TrigConfCool package
====================

The package contains the code to save an HLT prescale key to Cool for a given run and lb range. This is part of the procedure to update the HLT prescales during an ongoing run. The code is to be used by the CTP or anyone implementing the MasterTrigger interface.

Usage
-----

One has to instantiate a `PrescaleKey2CoolWriter` with the correct database connection and call the `write2cool(unsigned int run, unsigned int lb, unsigned int psk)` method. That writes the HLT prescale key `psk` for run `run` and lumiblock range `lb` to the end of the run. The key is written to the folder `TRIGGER/HLT/PrescaleKey`.

```
#include "TrigConfCool/PrescaleKey2CoolWriter.h"

TrigConf::PrescaleKey2CoolWriterDevTool pskwriter("COOLONL_TRIGGER/CONDBR2");

bool writeSuccess = pskwriter.write2cool(run, lb, psk)

```

The connection to the database only happens inside the `write2cool` function. At the end of that function the connection is closed again.


Cool db connection
------------------

### Point 1

At Point-1 the DB connection should be `"COOLONL_TRIGGER/CONDBR2"`. This should point to the write account `_W` in the dblookup and authentication files. 

### Testing

For testing purposes, e.g. on the testbed, one can also specify an sqlite file, e.g. `"hltpsk.db"` (it must be ending with `.db`).

One can also specify another oracle database, e.g. `"TRIGGERDBDEV2/CONDBR2"`. For this one might have to create the cool folder structure first, using the standalone application. 

### Permissions

For the creation of the cool database structure one has to connect as schema owner to the DB. This can be done most easily by adopting the corresponding `authentication.xml` file. For inserting PSKs the usual write permissions are sufficient (connect to the `_W` account).

Standalone application
----------------------

A standalone application `TestHltPsk2CoolWriting` exists that supports inspection, psk insertion and also the db and folder creation. Here an example how to use it


```
> TestHltPsk2CoolWriting hltpsk.db                                                    
PrescaleKey2CoolWriterDevTool created for database: sqlite://;schema=hltpsk.db;dbname=CONDBR2

1) ..... print content
2) ..... insert entry
3) ..... create db and folder
q) ..... Quit

Choice: 1
run (0 prints all)  : 0
Opening database 'sqlite://;schema=hltpsk.db;dbname=CONDBR2' [read-only]
--------------------------
|   run   |  lb  |  psk  |
--------------------------
|       1 |    1 |     1 |
|       1 |    5 |     2 |
|       1 |   16 |     3 |
|       1 |   22 |     5 |
|       2 |    0 |    13 |
|       2 |    1 |    19 |
|       3 |    0 |    16 |
--------------------------
Closing database 'sqlite://;schema=hltpsk.db;dbname=CONDBR2'
```

One can also use it to quickly print the HLT prescales for a certain run, e.g.

```
> TestHltPsk2CoolWriting COOLONL_TRIGGER/CONDBR2                                      
CORAL_DBLOOKUP_PATH = /eos/user/a/atdaqeos/adm/db
CORAL_AUTH_PATH = /eos/user/a/atdaqeos/adm/db
PrescaleKey2CoolWriterDevTool created for database: COOLONL_TRIGGER/CONDBR2

1) ..... print content
2) ..... insert entry
3) ..... create db and folder
q) ..... Quit

Choice: 1
run (0 prints all)  : 352394
Opening database 'COOLONL_TRIGGER/CONDBR2' [read-only]
--------------------------
|   run   |  lb  |  psk  |
--------------------------
|  352394 |    0 | 16257 |
|  352394 |   46 | 16303 |
|  352394 |   89 | 16406 |
|  352394 |   95 | 16397 |
|  352394 |  104 | 16364 |
|  352394 |  107 | 16304 |
...
```