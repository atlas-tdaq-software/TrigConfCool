tdaq_package()

tdaq_add_library(TrigConfCool
        src/PrescaleKey2CoolWriter.cxx src/PrescaleKey2CoolWriterDevTool.cxx
        LINK_LIBRARIES CORAL::CoralBase CORAL::CoralKernel COOL::CoolApplication COOL::CoolKernel rc_OnlSvc is tdaq-common::ers TTCInfo_ISINFO
)

tdaq_add_executable(TestHltPsk2CoolWriting
        src/test/TestHltPsk2CoolWriting.cxx
        LINK_LIBRARIES TrigConfCool
        )
