#include <iostream>
#include <limits>

#include "TrigConfCool/PrescaleKey2CoolWriterDevTool.h"

using std::cout;
using std::endl;
using std::cin;

unsigned int lastEnteredRun(0);

unsigned int
getInput(const std::string & inpName, unsigned int defaultVal = 0) {
   cin.clear();
   std::string in = "";
   unsigned int input;
   while (true) {
      // print input request
      cout << inpName;
      if (defaultVal != 0 ) {
         cout << " [<RET>=" << defaultVal << "]";
      }
      cout << " : ";

      // read input
      getline(cin, in);

      if (in.empty()) { // just return has been hit
         if( defaultVal == 0 ) {
            cout << "Please enter something!" << endl;
            cin.clear();
         } else {
            input = defaultVal;
            break;
         }
      } else { // a number has been given
         input = std::stoul(in);
         break;
      }
   }
   return input;
}

std::string
getInputString(const std::string & inpName, const std::string & defaultVal = "") {
   cin.clear();
   std::string in = "";
   std::string input;
   while (true) {
      // print input request
      cout << inpName;
      if (defaultVal != "" ) {
         cout << " [<RET>=" << defaultVal << "]";
      }
      cout << " : ";

      // read input
      getline(cin, in);

      if (in.empty()) { // just return has been hit
         if( defaultVal == "" ) {
            cout << "Please enter something!" << endl;
            cin.clear();
         } else {
            input = defaultVal;
            break;
         }
      } else { // a number has been given
         input = in;
         break;
      }
   }
   return input;
}





bool
ask(const std::string & question) {
   cout << question << " [y/N] ";
   std::string answer = "";
   getline(cin, answer);   
   return (answer == "Y") or (answer == "y");
}

void
printL1Psk(TrigConf::PrescaleKey2CoolWriterDevTool* pskwriter) {
   cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
   unsigned int run = getInput("run (0 prints all) ", lastEnteredRun);
   pskwriter->printL1PskContent(run);
   lastEnteredRun = run;
}

void
printHLTPsk(TrigConf::PrescaleKey2CoolWriterDevTool* pskwriter) {
   cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
   unsigned int run = getInput("run (0 prints all) ", lastEnteredRun);
   pskwriter->printHLTPskContent(run);
   lastEnteredRun = run;
}

void
printBgsk(TrigConf::PrescaleKey2CoolWriterDevTool* pskwriter) {
   cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
   unsigned int run = getInput("run (0 prints all) ", lastEnteredRun);
   pskwriter->printBgskContent(run);
   lastEnteredRun = run;
}

void
printSMK(TrigConf::PrescaleKey2CoolWriterDevTool* pskwriter) {
   cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
   unsigned int run = getInput("run (0 prints all) ", lastEnteredRun);
   pskwriter->printSMKContent(run);
   lastEnteredRun = run;
}

void
printConfigSource(TrigConf::PrescaleKey2CoolWriterDevTool* pskwriter) {
   pskwriter->printConfigSourceString();
}


void
insertKey(TrigConf::PrescaleKey2CoolWriterDevTool* pskwriter, const std::string & keyType) {
   std::string desc{};
   if(keyType == "L1PSK") {
      desc = "L1PS key ";
   } else if(keyType == "HLTPSK") {
      desc = "HLTPS key ";
   } else if(keyType == "BGSK") {
      desc = "BG key ";
   } else {
      return;
   }
   cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
   unsigned int run = getInput("run ", lastEnteredRun);
   unsigned int lb = getInput("lb  ");
   unsigned int key = getInput(desc);
   bool confirmed = ask("Do you want to insert " + desc + std::to_string(key) + " for run " + std::to_string(run) + " starting at lb " + std::to_string(lb));
   if (!confirmed) {
      cout << "ok, will do nothing" << endl;
      return;
   }
   if(keyType == "L1PSK") {
      pskwriter->writeL1Psk2Cool(run, lb, key);
   } else if(keyType == "HLTPSK") {
      pskwriter->writeHLTPsk2Cool(run, lb, key);
   } else if(keyType == "BGSK") {
      pskwriter->writeBgsk2Cool(run, lb, key);      
   }
   lastEnteredRun = run;
}

void
insertL1Psk(TrigConf::PrescaleKey2CoolWriterDevTool* pskwriter) {
   insertKey(pskwriter, "L1PSK");
}

void
insertHLTPsk(TrigConf::PrescaleKey2CoolWriterDevTool* pskwriter) {
   insertKey(pskwriter, "HLTPSK");
}

void
insertBgsk(TrigConf::PrescaleKey2CoolWriterDevTool* pskwriter) {
   insertKey(pskwriter, "BGSK");
}


void
insertSMK(TrigConf::PrescaleKey2CoolWriterDevTool* pskwriter) {
   cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
   unsigned int run = getInput("run ", lastEnteredRun);
   unsigned int smk = getInput("smk ");
   bool confirmed = ask("Do you want to insert SMK " + std::to_string(smk) + " for run " + std::to_string(run));
   if (!confirmed) {
      cout << "ok, will do nothing" << endl;
      return;
   }
   pskwriter->writeSmk2Cool(run, smk);
   lastEnteredRun = run;
}

/**
   IS interaction
 */

void
printISContent(TrigConf::PrescaleKey2CoolWriterDevTool* pskwriter) {
   pskwriter->printISInfo();
}


void
publishSmk2IS(TrigConf::PrescaleKey2CoolWriterDevTool* pskwriter) {
   cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
   unsigned int smk = getInput("SMK ");
   pskwriter->publishSmk2IS(smk);
}

void
publishL1Psk2IS(TrigConf::PrescaleKey2CoolWriterDevTool* pskwriter) {
   cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
   unsigned int psk = getInput("L1 PSK ");
   pskwriter->publishL1Psk2IS(psk);
}

void
publishHLTPsk2IS(TrigConf::PrescaleKey2CoolWriterDevTool* pskwriter) {
   cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
   unsigned int psk = getInput("HLT PSK ");
   pskwriter->publishHltPsk2IS(psk);
}

void
publishBgsk2IS(TrigConf::PrescaleKey2CoolWriterDevTool* pskwriter) {
   cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
   unsigned int bgsk = getInput("BGSK ");
   pskwriter->publishBgsk2IS(bgsk);
}

void
publishRelease2IS(TrigConf::PrescaleKey2CoolWriterDevTool* pskwriter) {
   cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
   std::string release = getInputString("Release ");
   pskwriter->publishRelease2IS(release);
}

void
create(TrigConf::PrescaleKey2CoolWriterDevTool* pskwriter) {
   pskwriter->createDBandFolder();

}

char printCoolDialog( TrigConf::PrescaleKey2CoolWriterDevTool* pskw )
{

   char c;
   while(c!='q' and c!='m') {  
      c='q';
      cout << endl;
      cout << "Printing COOL DB content" << endl;
      cout << "========================" << endl;
      cout << "   1) ..... for SMK and release version" << endl;
      cout << "   2) ..... for L1 prescale set key" << endl;
      cout << "   3) ..... for HLT prescale set key" << endl;
      cout << "   4) ..... for L1 bunchgroup set key" << endl << endl;
      cout << "   m) ..... main dialog" << endl;
      cout << "   q) ..... quit" << endl << endl;
      cout << "   Choice: "; cin >> c;
      switch(c){
      case '1': printSMK(pskw); break;
      case '2': printL1Psk(pskw); break; 
      case '3': printHLTPsk(pskw); break; 
      case '4': printBgsk(pskw); break; 
      default: break; 
      }
   }
   return c;
}

char insert2CoolDialog( TrigConf::PrescaleKey2CoolWriterDevTool* pskw )
{

   char c;
   while(c!='q' and c!='m') {  
      c='q';
      cout << endl;
      cout << "Insert into COOL DB" << endl;
      cout << "===================" << endl;
      cout << "   1) ..... SMK and release version" << endl;
      cout << "   2) ..... L1 prescale set key" << endl;
      cout << "   3) ..... HLT prescale set key" << endl;
      cout << "   4) ..... L1 bunchgroup set key" << endl << endl;
      cout << "   m) ..... main dialog" << endl;
      cout << "   q) ..... quit" << endl << endl;
      cout << "   Choice: "; cin >> c;
      switch(c){
      case '1': insertSMK(pskw); break; 
      case '2': insertL1Psk(pskw); break; 
      case '3': insertHLTPsk(pskw); break; 
      case '4': insertBgsk(pskw); break; 
      default: break; 
      }
   }
   return c;
}

char isDialog( TrigConf::PrescaleKey2CoolWriterDevTool* pskw )
{

   char c;
   while(c!='q' and c!='m') {  
      c='q';
      cout << endl;
      cout << "IS Menu" << endl;
      cout << "=======" << endl;
      cout << "   1) ..... Publish SMK" << endl;
      cout << "   2) ..... Publish L1 prescale set key" << endl;
      cout << "   3) ..... Publish HLT prescale set key" << endl;
      cout << "   4) ..... Publish L1 bunchgroup set key" << endl;
      cout << "   5) ..... Publish Release" << endl << endl;
      cout << "   6) ..... Print IS information" << endl << endl;
      cout << "   m) ..... main dialog" << endl;
      cout << "   q) ..... quit" << endl << endl;
      cout << "   Choice: "; cin >> c;
      switch(c){
      case '1': publishSmk2IS(pskw); break; 
      case '2': publishL1Psk2IS(pskw); break; 
      case '3': publishHLTPsk2IS(pskw); break; 
      case '4': publishBgsk2IS(pskw); break; 
      case '5': publishRelease2IS(pskw); break; 
      case '6': printISContent(pskw); break; 
      default: break; 
      }
   }
   return c;
}




void mainDialog( TrigConf::PrescaleKey2CoolWriterDevTool* pskw )
{

   char c;
   while(c!='q') {  
      c='q';
      cout << endl;
      cout << "Main Menu" << endl;
      cout << "=========" << endl;
      cout << "   1) ..... print COOL db content" << endl;
      cout << "   2) ..... insert information into COOL" << endl;
      cout << "   3) ..... print config source (release version, etc) - requires partition" << endl << endl;
      cout << "   5) ..... IS information" << endl << endl;
      cout << "   9) ..... create COOL db and folders (for testing on private DBs)" << endl << endl;
      cout << "   q) ..... quit" << endl << endl;
      cout << "   Choice: "; cin >> c;
      switch(c){
      case '1': if( printCoolDialog(pskw) == 'q' ) { c='q'; }; break; 
      case '2': if( insert2CoolDialog(pskw) == 'q' ) { c='q'; }; break; 
      case '3': printConfigSource(pskw); break;
      case '5': if( isDialog(pskw) == 'q' ) { c='q'; }; break; 
      case '9': create(pskw); break; 
      default: break; 
      }
   }

}




int main(int argc, char** argv) {
   if(argc!=2 || std::string(argv[1])=="-h" || std::string(argv[1])=="--help" ) {
      cout << "usage:" << endl
           << argv[0] << " <cooldb>" << endl << endl;
      cout << "examples: " << endl
           << "   " << argv[0] << "  hltpsk.db                   - sqlite file for testing" << endl
           << "   " << argv[0] << "  COOLONL_TRIGGER/CONDBR2     - offline replica of online db" << endl
           << "   " << argv[0] << "  TRIGGERDBDEV2/CONDBR2       - for testing on oracle" << endl;
      return 1;
   }

   std::string dbconn(argv[1]);
   
   TrigConf::PrescaleKey2CoolWriterDevTool pskw(dbconn);

   mainDialog(&pskw);

   return 0;
}
