/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "TrigConfCool/PrescaleKey2CoolWriterDevTool.h"

#include "CoralBase/Exception.h"
#include "CoolKernel/IDatabaseSvc.h"
#include "CoolKernel/Record.h"
#include "CoolKernel/FolderSpecification.h"
#include "CoolKernel/IFolder.h"
#include "CoolKernel/IObjectIterator.h"
#include "CoolKernel/IObject.h"
#include "CoolApplication/DatabaseSvcFactory.h"

#include "RunControl/Common/OnlineServices.h"
#include "ipc/core.h"


#include <cstdlib>
#include <iostream>
#include <iomanip>


using std::cout;
using std::cerr;
using std::endl;


namespace {

   bool endsWith(const std::string& str, const std::string& suffix)
   {
      return str.size() >= suffix.size() && 0 == str.compare(str.size()-suffix.size(), suffix.size(), suffix);
   }

   bool
   createFolderIfNotExist( cool::IDatabasePtr db,
                           const std::string & folder,
                           const cool::FolderSpecification & fspec,
                           bool isMultiChannel )
   {
      static const std::string singleChannelDesc = "<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"40774348\" /></addrHeader><typeName>AthenaAttributeList</typeName>";
      static const std::string multiChannelDesc  = "<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"1238547719\" /></addrHeader><typeName>CondAttrListCollection</typeName>";

      if( db->existsFolder( folder ) )
         return false;

      cool::IFolderPtr ptr = db->createFolder( folder, fspec,
                                               isMultiChannel ? multiChannelDesc : singleChannelDesc,
                                               /*createParents=*/ true );

      return true;
   }
}




TrigConf::PrescaleKey2CoolWriterDevTool::PrescaleKey2CoolWriterDevTool(const std::string & dbalias, bool doTestConnection) :
   PrescaleKey2CoolWriter(dbalias)
{

   if( endsWith(dbalias,std::string(".db")) ) {
      m_dbalias = std::string("sqlite://;schema=") + dbalias + ";dbname=CONDBR2";
   } else {
      if( const char* env_p = std::getenv("CORAL_DBLOOKUP_PATH")) {
         cout << "CORAL_DBLOOKUP_PATH = " << env_p << endl;
      } else {
         cout << "No CORAL_DBLOOKUP_PATH defined" << endl;
      }
      if( const char* env_p = std::getenv("CORAL_AUTH_PATH")) {
         cout << "CORAL_AUTH_PATH = " << env_p << endl;
      } else {
         cout << "No CORAL_AUTH_PATH defined" << endl;
      }
   }

   cout << "PrescaleKey2CoolWriterDevTool created for database: " << m_dbalias << endl;

   if( doTestConnection ) {
      try {
         openDb(/*readonly*/ true);
         closeDb();
      }
      catch(std::exception & e) {
         std::cerr << e.what() << endl;
         std::cerr << "Could not open database " << m_dbalias << ", please try to create one (option 3)" << endl;
      }
   }
}


TrigConf::PrescaleKey2CoolWriterDevTool::~PrescaleKey2CoolWriterDevTool()
{}


void
TrigConf::PrescaleKey2CoolWriterDevTool::createDBandFolder()
{
   cool::IDatabaseSvc& dbSvc = cool::DatabaseSvcFactory::databaseService();

   cool::IDatabasePtr db;

   bool needToCreateDb = false;

   // opening database or create it
   try {

      db = dbSvc.openDatabase( m_dbalias, /*readOnly=*/ false ); 

   } catch(cool::DatabaseDoesNotExist & e) {

      needToCreateDb = true;

   } catch(cool::Exception & e) {
      std::cerr << "*** COOL exception caught: " << e.what() << endl;
      std::cerr << "Couldn't open database " << m_dbalias << " although it exists" << endl;
      return;
   } catch(coral::Exception & e) {
      std::cerr << "*** CORAL exception caught: " << e.what() << endl;
      std::cerr << "Couldn't open database " << m_dbalias << " although it exists" << endl;
      return;
   } catch(std::exception & e) {
      std::cerr << "*** std::exception caught: " << e.what() << endl;
      std::cerr << "Couldn't open database " << m_dbalias << " although it exists" << endl;
      return;
   } 

   if (needToCreateDb) {
      try {
         db = dbSvc.createDatabase( m_dbalias );
      } catch(cool::Exception & e) {
         std::cerr << "*** COOL exception caught: " << e.what() << endl;
         std::cerr << "Couldn't create database " << m_dbalias << endl;
         return;
      } catch(coral::Exception & e) {
         std::cerr << "*** CORAL exception caught: " << e.what() << endl;
         std::cerr << "Couldn't create database " << m_dbalias << endl;
         return;
      } catch(std::exception & e) {
         std::cerr << "*** std::exception caught: " << e.what() << endl;
         std::cerr << "Couldn't create database " << m_dbalias << endl;
         return;
      } 
   }


   auto folderDefs = std::vector<std::pair<std::string, cool::FolderSpecification>>();

   {
      // L1 PSK folder
      cool::RecordSpecification rspec;
      rspec.extend( "Lvl1PrescaleConfigurationKey", cool::StorageType::UInt32 );
      cool::FolderSpecification fspec( cool::FolderVersioning::MULTI_VERSION, rspec );
      folderDefs.push_back({m_l1pskFolderName,fspec});
   }
   {
      // HLT PSK folder
      cool::RecordSpecification rspec;
      rspec.extend( "HltPrescaleKey", cool::StorageType::UInt32 );
      cool::FolderSpecification fspec( cool::FolderVersioning::MULTI_VERSION, rspec );
      folderDefs.push_back({m_hltpskFolderName,fspec});
   }
   {
      // BGSK folder
      cool::RecordSpecification rspec;
      rspec.extend( "Lvl1BunchGroupConfigurationKey", cool::StorageType::UInt32 );
      cool::FolderSpecification fspec( cool::FolderVersioning::MULTI_VERSION, rspec );
      folderDefs.push_back({m_bgskFolderName,fspec});
   }
   {
      // SMK folder
      cool::RecordSpecification rspec;
      rspec.extend( "MasterConfigurationKey", cool::StorageType::UInt32 );
      rspec.extend( "HltPrescaleConfigurationKey", cool::StorageType::UInt32 );
      rspec.extend( "ConfigSource", cool::StorageType::String255 );
      cool::FolderSpecification fspec( cool::FolderVersioning::SINGLE_VERSION, rspec );
      folderDefs.push_back({m_smkFolderName,fspec});
   }


   for( auto & def : folderDefs ) {
      auto & fName = def.first;
      auto & fSpec = def.second;

      bool isNewFolder = createFolderIfNotExist(db, fName, fSpec, /*isMultiChannel=*/ false);

      if ( isNewFolder ) {
         cout << "Created database " << m_dbalias << " and folder " << fName << endl;
      } else {
         cout << "Database " << m_dbalias << " and folder " << fName << " existed already " << endl;
      }
   }
   db->closeDatabase();
}


bool
TrigConf::PrescaleKey2CoolWriterDevTool::printL1PskContent(unsigned int runnr) {
   return printContent(runnr, "L1PSK");
}

bool
TrigConf::PrescaleKey2CoolWriterDevTool::printHLTPskContent(unsigned int runnr) {
   return printContent(runnr, "HLTPSK");
}

bool
TrigConf::PrescaleKey2CoolWriterDevTool::printBgskContent(unsigned int runnr) {
   return printContent(runnr, "BGSK");
}

bool
TrigConf::PrescaleKey2CoolWriterDevTool::printContent(unsigned int runnr, const std::string & level) {

   std::string folderName{};
   std::string attribute{};

   if(level == "L1PSK") {
      folderName = m_l1pskFolderName;
      attribute = "Lvl1PrescaleConfigurationKey";
   } else if (level == "HLTPSK") {
      folderName = m_hltpskFolderName;
      attribute = "HltPrescaleKey";
   } else if (level == "BGSK") {
      folderName = m_bgskFolderName;
      attribute = "Lvl1BunchGroupConfigurationKey";
   } else {
      return false;
   }

   // IOV is from given LB to the end of the run
   cool::ValidityKey since = cool::ValidityKeyMin;
   cool::ValidityKey until = cool::ValidityKeyMax;

   if ( runnr > 0 ) {
      since = runnr; since <<= 32;
      until = runnr + 1; until <<= 32; until -= 1;
   }

   cool::IDatabasePtr db;
   try {
      openDb( /*readonly=*/ true);
   }
   catch(...) {
      closeDb();
      return false;
   }

   cool::IFolderPtr folder;
   try {
      folder = m_dbPtr->getFolder( folderName );
   }
   catch (std::exception & e) {
      cerr << "Could not access folder " << folderName << endl;
      cerr << e.what() << endl;
      closeDb();
      return false;
   }

   cool::IObjectIteratorPtr objects = folder->browseObjects( since, until, 0 );

   cout << "--------------------------" << endl;
   cout << "|   run   |  lb  |  psk  |" << endl;
   cout << "--------------------------" << endl;

   while(objects->goToNext()) {
      const cool::IObject& obj = objects->currentRef();
      const cool::IRecord & payload = obj.payload();
      cout << "| " << std::setw(7) << (obj.since() >> 32) << " | " << std::setw(4) << (obj.since() & 0xFFFFFFFF) << " | " << std::setw(5) << payload[attribute].data<cool::UInt32>() << " |" << endl;
   }

   cout << "--------------------------" << endl;

   closeDb();

   return true;
}


bool
TrigConf::PrescaleKey2CoolWriterDevTool::printSMKContent(unsigned int runnr) {

   // IOV is from given LB to the end of the run
   cool::ValidityKey since = cool::ValidityKeyMin;
   cool::ValidityKey until = cool::ValidityKeyMax;

   if ( runnr > 0 ) {
      since = runnr; since <<= 32;
      until = runnr + 1; until <<= 32; until -= 1;
   }

   cool::IDatabasePtr db;
   try {
      openDb( /*readonly=*/ true);
   }
   catch(...) {
      closeDb();
      return false;
   }

   cool::IFolderPtr folder;
   try {
      folder = m_dbPtr->getFolder( m_smkFolderName );
   }
   catch (std::exception & e) {
      cerr << "Could not access folder " << m_smkFolderName << endl;
      cerr << e.what() << endl;
      closeDb();
      return false;
   }

   cool::IObjectIteratorPtr objects = folder->browseObjects( since, until, 0 );

   cout << "-----------------------------------------------------" << endl;
   cout << "|   run   |  smk  |             release             |" << endl;
   cout << "-----------------------------------------------------" << endl;

   while(objects->goToNext()) {
      const cool::IObject& obj = objects->currentRef();
      const cool::IRecord & payload = obj.payload();
      unsigned int runNr = obj.since() >> 32;
      if(runNr > 1000000000) { // there are some fake entries from 2014
         break;
      }
      cout << "| " << std::setw(7) << (obj.since() >> 32)
           << " | " << std::setw(5) << payload["MasterConfigurationKey"].data<cool::UInt32>() 
           << " | " << std::setw(31) << payload["ConfigSource"].data<cool::String255>() 
           << " |" << endl;
   }

   cout << "-----------------------------------------------------" << endl;

   closeDb();

   return true;
}

void
TrigConf::PrescaleKey2CoolWriterDevTool::printConfigSourceString()
{
   auto cfgSource = getConfigSource();
   cout << "Config source string: " << cfgSource << endl;
}

void
TrigConf::PrescaleKey2CoolWriterDevTool::printISInfo()
{
   unsigned int smk {0};
   unsigned int l1psk{0};
   unsigned int hltpsk {0};
   unsigned int bgsk {0};
   std::string release {};

   getInfoFromIS( smk, l1psk, hltpsk, bgsk, release );

   cout << "Current SMK: " << smk << endl;
   cout << "Current L1 PSK: " << l1psk << endl;
   cout << "Current HLT PSK: " << hltpsk << endl;
   cout << "Current L1 BGSK: " << bgsk << endl;
   cout << "Current HLT release: " << release << endl;
}

