/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "TrigConfCool/PrescaleKey2CoolWriter.h"

#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/Exceptions.h"

#include <dal/Partition.h>
#include <dal/TriggerConfiguration.h>
#include <dal/TriggerDBConnection.h>
#include <dal/Variable.h>
#include <dal/Segment.h>

#include "is/infodictionary.h"
#include "is/exceptions.h"

#include "ipc/partition.h"

#include "CoralBase/Exception.h"
#include "CoolKernel/IDatabaseSvc.h"
#include "CoolKernel/Record.h"
#include "CoolKernel/FolderSpecification.h"
#include "CoolKernel/IFolder.h"
#include "CoolKernel/IObjectIterator.h"
#include "CoolKernel/IObject.h"
#include "CoolApplication/DatabaseSvcFactory.h"

#include "TTCInfo/TrigConfSmKeyNamed.h"
#include "TTCInfo/TrigConfL1PsKeyNamed.h"
#include "TTCInfo/TrigConfHltPsKeyNamed.h"
#include "TTCInfo/TrigConfL1BgKeyNamed.h"
#include "TTCInfo/TrigConfReleaseNamed.h"


#include "ers/ers.h"

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

TrigConf::PrescaleKey2CoolWriter::PrescaleKey2CoolWriter(const std::string & dbalias) :
   m_dbalias(dbalias)
{}


TrigConf::PrescaleKey2CoolWriter::~PrescaleKey2CoolWriter()
{}


bool
TrigConf::PrescaleKey2CoolWriter::dbIsOpen() const {
   return m_dbPtr.use_count()>0 && m_dbPtr->isOpen();
}


void
TrigConf::PrescaleKey2CoolWriter::openDb( bool readOnly ) {

   if( dbIsOpen() ) {
      return;
   }

   cool::IDatabaseSvc& dbSvc = cool::DatabaseSvcFactory::databaseService();
   try {
      m_dbPtr = dbSvc.openDatabase( m_dbalias, readOnly );
      ERS_LOG( "Opening database '" << m_dbalias << "' [" << ( readOnly ? "read-only" : "read-write" ) << "]");
   } catch(std::exception& e) {
      TrigConf::DBConnectionIssue issue(ERS_HERE, "Could not open db " + m_dbalias, e.what());
      ers::error(issue);
      throw issue;
   }
}


void
TrigConf::PrescaleKey2CoolWriter::closeDb() {
   try {
      if(dbIsOpen()) {
         m_dbPtr->closeDatabase();
         ERS_LOG("Closing database '" << m_dbPtr->databaseId() << "'");
      }
   } catch(std::exception& e) {
      TrigConf::DBConnectionIssue issue(ERS_HERE, "Problem closing db " + m_dbPtr->databaseId(), e.what());
      ers::error(issue);
   }
   return;
}


bool
TrigConf::PrescaleKey2CoolWriter::writeL1Psk2Cool(unsigned int runnr, unsigned int lb, unsigned int l1psk)
{
   KeysCollection keys;
   keys.l1psk = l1psk;
   return writeKeys2Cool(keys, runnr, lb);
}

bool
TrigConf::PrescaleKey2CoolWriter::writeHLTPsk2Cool(unsigned int runnr, unsigned int lb, unsigned int hltpsk)
{
   KeysCollection keys;
   keys.hltpsk = hltpsk;
   return writeKeys2Cool(keys, runnr, lb);
}

bool
TrigConf::PrescaleKey2CoolWriter::writeBothPsk2Cool(unsigned int runnr, unsigned int lb, unsigned int l1psk, unsigned int hltpsk)
{
   KeysCollection keys;
   keys.l1psk = l1psk;
   keys.hltpsk = hltpsk;
   return writeKeys2Cool(keys, runnr, lb);
}

bool
TrigConf::PrescaleKey2CoolWriter::writeBgsk2Cool(unsigned int runnr, unsigned int lb, unsigned int bgsk)
{
   KeysCollection keys;
   keys.bgsk = bgsk;
   return writeKeys2Cool(keys, runnr, lb);
}


bool
TrigConf::PrescaleKey2CoolWriter::writeKeys2Cool(const KeysCollection keysColl, unsigned int runnr, unsigned int lb)
{

   std::vector<std::string> folderNames = { m_l1pskFolderName, m_hltpskFolderName, m_bgskFolderName};
   std::vector<std::string> attributes = { "Lvl1PrescaleConfigurationKey", "HltPrescaleKey", "Lvl1BunchGroupConfigurationKey"};
   std::vector<std::string> descriptions = { "L1PS key ", "HLTPS key ", "BG key "};
   std::vector<uint32_t> keys = { keysColl.l1psk, keysColl.hltpsk, keysColl.bgsk }; 

   // IOV is from given LB to the end of the run
   cool::ValidityKey since = runnr; since <<= 32; since += lb;
   cool::ValidityKey until = runnr + 1; until <<= 32;

   try {
      openDb( /*readonly=*/ false);
   }
   catch(...) {
      closeDb();
      return false;
   }

   bool errorEncountered {false};

   for(size_t idx = 0; idx<3; idx++) {
      const uint32_t key = keys[idx];

      if( key == 0 ) {
         continue;
      }

      const std::string folderName = folderNames[idx];
      const std::string attribute = attributes[idx];
      const std::string desc = descriptions[idx];

      cool::IFolderPtr folder;
      try {
         folder = m_dbPtr->getFolder( folderName );
      }
      catch (std::exception & e) {
         TrigConf::FolderAccessIssue issue(ERS_HERE, "Problem accessing folder " + folderName + ", won't be writing " + desc, e.what());
         ers::error(issue);
         errorEncountered = true;
         continue;
      }

      cool::Record ins_payload( folder->payloadSpecification() );
      ins_payload[attribute].setValue<cool::UInt32>(key);
      
      // check if empty to prevent accidental overwrites
      cool::IObjectIteratorPtr objects = folder->browseObjects( since, until, 0 );
      bool foundExistingRecord { false };
      while(objects->goToNext()) {
         const cool::IObject& obj = objects->currentRef();
         const cool::IRecord & payload = obj.payload();
         if( (obj.since()&0xFFFFFFFF) > lb) {
            TrigConf::DataInsertionIssue issue( ERS_HERE, 
                                                "Can't insert " + desc + std::to_string(key) + 
                                                " for run " + std::to_string(runnr) + " and start lb " + std::to_string(lb),
                                                ("An entry for this run with a higher lb already exists (lb " +
                                                 std::to_string((obj.since()&0xFFFFFFFF)) + ", hlt psk " + 
                                                 std::to_string(payload[attribute].data<cool::UInt32>()) + ")").c_str() );
            ers::error(issue);
            foundExistingRecord = true;
            break;
         }
      }
      if( foundExistingRecord ) {
         errorEncountered = true;
         continue;
      }
      
      folder->storeObject(since, until, ins_payload, 0 );
      ERS_INFO("Wrote " + desc << key << " for run " << runnr << " starting with lb " << lb << " to folder " << folderName); 
   }

   closeDb();

   return ! errorEncountered;
}

daq::rc::OnlineServices&
onlineServices()
{
   if( not IPCCore::isInitialised()) {
      try {
         IPCCore::init({});
      }
      catch(ers::Issue & ex) {
         ers::warning(ers::Message(ERS_HERE, ex));
      }
   }

   try {
      daq::rc::OnlineServices::instance();
   } 
   catch(daq::rc::NotInitialized & e) {
      if(std::getenv("TDAQ_PARTITION") == nullptr) {
         TrigConf::PartitionConfigParsingIssue issue(ERS_HERE, "No partition found. Can't determine HLT setup.", "environment var TDAQ_PARTITION does not exist");
         ers::error(issue);
      } else {
         daq::rc::OnlineServices::initialize(std::getenv("TDAQ_PARTITION"), "");
      }
   }

   return daq::rc::OnlineServices::instance();
}


bool
TrigConf::PrescaleKey2CoolWriter::findHLTSetup(std::string& releaseVersion, std::string& asetupArgs) const {

   releaseVersion="";
   asetupArgs="";

   Configuration & config = onlineServices().getConfiguration();

   if( config.get<daq::core::Segment>("HLT") ) { // only look for HLT setup if also an HLT segment exists
     
      vector<const daq::core::Variable *> variables;
      config.get(variables);
      for(auto var : variables) {
         if( var->UID() == "HLT_VERSION" ) {
            releaseVersion = var->get_Value();
         }
         if( var->UID() == "HLT_ASETUP_ARGS"  ) {
            asetupArgs = var->get_Value();
         }
      }

      if( releaseVersion != "" ) {
         ERS_INFO("HLT is running with HLT version " << releaseVersion);
      } else {
         TrigConf::PartitionConfigParsingIssue issue(ERS_HERE, "HLT segment is enabled but not HLT version found", "");
         ers::error(issue);
         return false;
      }

   }
   return true;
}

bool
TrigConf::PrescaleKey2CoolWriter::findTriggerDBConnectionAlias(std::string& dbalias) const {
   dbalias = "";
   const daq::core::Partition& partition = onlineServices().getPartition();
   const daq::core::TriggerConfiguration * tc = partition.get_TriggerConfiguration();
   if( tc == nullptr ) {
      TrigConf::PartitionConfigParsingIssue issue(ERS_HERE, "Partition does not have a TriggerConfiguration", "");
      ers::error(issue);
      return false;
   }
   const daq::core::TriggerDBConnection * dbconn = tc->get_TriggerDBConnection();
   if( dbconn == nullptr ) {
      TrigConf::PartitionConfigParsingIssue issue(ERS_HERE, "TriggerConfiguration " + tc->UID() + " does not have a TriggerDBConnection", "");
      ers::error(issue);
      return false;
   }
   dbalias = dbconn->get_Alias();
   ERS_INFO("Trigger DB connection alias is '" << dbalias << "'");
   return true;
}


std::string
TrigConf::PrescaleKey2CoolWriter::getConfigSource() const
{
   std::string releaseVersion{};
   std::string asetupArgs{};
   std::string dbalias{};

   findHLTSetup(releaseVersion, asetupArgs);
   findTriggerDBConnectionAlias(dbalias);
   
   // build the cfgSource string 
   // Run 2 format was "dbalias,release,project"
   // Run 3 format is "dbalias;release;asetupArgs"

   std::string cfgSource = dbalias + ";" + releaseVersion + ";" + asetupArgs;

   return cfgSource;
}



bool
TrigConf::PrescaleKey2CoolWriter::writeSmk2Cool(unsigned int runnr, unsigned int smk)
{

   const std::string configSource = getConfigSource();

   // IOV is from given LB to the end of the run
   cool::ValidityKey since = runnr; since <<= 32;
   cool::ValidityKey until = runnr + 1; until <<= 32;

   try {
      openDb( /*readonly=*/ false);
   }
   catch(...) {
      closeDb();
      return false;
   }

   cool::IFolderPtr folder;
   try {
      folder = m_dbPtr->getFolder( m_smkFolderName );
   }
   catch (std::exception & e) {
      closeDb();
      TrigConf::FolderAccessIssue issue(ERS_HERE, "Problem accessing folder " + m_smkFolderName, e.what());
      ers::error(issue);
      return false;
   }

   cool::Record payload( folder->payloadSpecification() );
   payload["MasterConfigurationKey"].setValue<cool::UInt32>(smk);
   payload["HltPrescaleConfigurationKey"].setValue<cool::UInt32>(0); // this field is unused
   payload["ConfigSource"].setValue<cool::String255>(configSource);


   // check if empty to prevent accidental overwrites
   cool::IObjectIteratorPtr objects = folder->browseObjects( since, until, 0 );
   while(objects->goToNext()) {
      const cool::IObject& obj = objects->currentRef();
      const cool::IRecord & payload = obj.payload();
      TrigConf::DataInsertionIssue issue( ERS_HERE, 
                                          "Can't insert smk " + std::to_string(smk) + 
                                          " for run " + std::to_string(runnr),
                                          ("An entry for this run already exists, smk " + 
                                           std::to_string(payload["MasterConfigurationKey"].data<cool::UInt32>()) + ")").c_str() );
      closeDb();
      ers::error(issue);
      return false;
   }

   folder->storeObject(since, until, payload, 0 );
   ERS_INFO("Wrote smk " << smk << " and config source '" << configSource << "' for run " << runnr << "to folder " << m_smkFolderName); 

   closeDb();

   return true;
}

void
TrigConf::PrescaleKey2CoolWriter::getInfoFromIS( unsigned int & smk,
                                                 unsigned int & l1psk,
                                                 unsigned int & hltpsk,
                                                 unsigned int & bgsk,
                                                 std::string & release )
{

   auto & part = onlineServices().getIPCPartition();
   
   TrigConfSmKeyNamed    is_smk( part, m_isLocSMK );
   TrigConfL1PsKeyNamed  is_l1psk( part , m_isLocL1PSK );
   TrigConfHltPsKeyNamed is_hltpsk( part, m_isLocHLTPSK );
   TrigConfL1BgKeyNamed  is_bgsk( part, m_isLocBGSK );
   TrigConfReleaseNamed  is_rel( part, m_isLocRel );

   try {
      is_smk.checkout();
      smk = is_smk.SuperMasterKey;
   } catch (daq::is::NotFound & e) {
      ERS_LOG("IS Info " << is_smk.name() << " not available. " << e.what());
      smk = 0;
   }

   try {
      is_l1psk.checkout();
      l1psk = is_l1psk.L1PrescaleKey;
   } catch (daq::is::NotFound & e) {
      ERS_LOG("IS Info " << is_l1psk.name() << " not available. " << e.what());
      l1psk = 0;
   }

   try {
      is_hltpsk.checkout();
      hltpsk = is_hltpsk.HltPrescaleKey;
   } catch (daq::is::NotFound & e) {
      ERS_LOG("IS Info " << is_hltpsk.name() << " not available. " << e.what());
      hltpsk = 0;
   }

   try {
      is_bgsk.checkout();
      bgsk = is_bgsk.L1BunchGroupKey;
   } catch (daq::is::NotFound & e) {
      ERS_LOG("IS Info " << is_bgsk.name() << " not available. " << e.what());
      bgsk = 0;
   }

   try {
      is_rel.checkout();
      release = is_rel.HLTReleaseVersion;
   } catch (daq::is::NotFound & e) {
      ERS_LOG("IS Info " << is_rel.name() << " not available. " << e.what());
      release = "";
   }

}

bool
TrigConf::PrescaleKey2CoolWriter::publishSmk2IS(unsigned int smk, const std::string & comment) {
   TrigConfSmKeyNamed is_smk( onlineServices().getIPCPartition(), m_isLocSMK );
   is_smk.SuperMasterKey = smk;
   is_smk.SuperMasterComment = comment;
   return publish2IS(is_smk);
}

bool
TrigConf::PrescaleKey2CoolWriter::publishL1Psk2IS(unsigned int l1psk, const std::string & comment) {
   TrigConfL1PsKeyNamed is_l1psk( onlineServices().getIPCPartition(), m_isLocL1PSK );
   is_l1psk.L1PrescaleKey = l1psk;
   is_l1psk.L1PrescaleComment = comment;
   return publish2IS(is_l1psk);
}

bool
TrigConf::PrescaleKey2CoolWriter::publishHltPsk2IS(unsigned int hltpsk, const std::string & comment) {
   TrigConfHltPsKeyNamed is_hltpsk( onlineServices().getIPCPartition(), m_isLocHLTPSK );
   is_hltpsk.HltPrescaleKey = hltpsk;
   is_hltpsk.HltPrescaleComment = comment;
   return publish2IS(is_hltpsk);
}

bool
TrigConf::PrescaleKey2CoolWriter::publishBgsk2IS(unsigned int bgsk, const std::string & comment) {
   TrigConfL1BgKeyNamed is_bgsk( onlineServices().getIPCPartition(), m_isLocBGSK );
   is_bgsk.L1BunchGroupKey = bgsk;
   is_bgsk.L1BunchGroupComment = comment;
   return publish2IS(is_bgsk);
}

bool
TrigConf::PrescaleKey2CoolWriter::publishRelease2IS(const std::string & release, const std::string & project) {
   TrigConfReleaseNamed is_rel( onlineServices().getIPCPartition(), m_isLocRel );
   is_rel.HLTReleaseVersion = release;
   is_rel.HLTPatchProject = project;
   return publish2IS(is_rel);
}

bool
TrigConf::PrescaleKey2CoolWriter::publish2IS( ISNamedInfo & isinfo ) {
   try {
      isinfo.checkin();
      ERS_LOG("Published to IS:" << endl << isinfo);
   } catch (std::exception & ex) {
      TrigConf::ISPublicationIssue issue( ERS_HERE, "Can't publish " + isinfo.name() + " in IS", ex.what() );
      ers::error(issue);
      return false;
   }
   return true;
}
