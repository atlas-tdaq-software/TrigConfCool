/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TRIGCONFCOOL_PRESCALEKEY2COOLWRITERDEVTOOL
#define TRIGCONFCOOL_PRESCALEKEY2COOLWRITERDEVTOOL

#include "PrescaleKey2CoolWriter.h"

namespace TrigConf {

   class PrescaleKey2CoolWriterDevTool : public PrescaleKey2CoolWriter {
   public:

      PrescaleKey2CoolWriterDevTool() = delete;

      PrescaleKey2CoolWriterDevTool(const std::string & dbalias, bool doTestConnection = false);
   
      virtual ~PrescaleKey2CoolWriterDevTool();

      void createDBandFolder();

      /** Print COOL content of L1 PSK folder */
      bool printL1PskContent(unsigned int runnr = 0);

      /** Print COOL content of HLT PSK folder */
      bool printHLTPskContent(unsigned int runnr = 0);

      /** Print COOL content of Bunchgroupset key folder */
      bool printBgskContent(unsigned int runnr = 0);

      /** Print COOL content of SMK folder */
      bool printSMKContent(unsigned int runnr = 0);

      /** config source string that is written to COOL */
      void printConfigSourceString();

      /** print IS info */
      void printISInfo();

   private:
      /** Print COOL content of an PSK folder */
      bool printContent(unsigned int runnr, const std::string & level );
      
   };

}

#endif
