/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TRIGCONFCOOL_PRESCALEKEY2COOLWRITER
#define TRIGCONFCOOL_PRESCALEKEY2COOLWRITER

#include "CoolKernel/IDatabase.h"

#include "ers/ers.h"

#include <string>

class ISNamedInfo;

ERS_DECLARE_ISSUE( TrigConf,
                   PSK2COOLIssue,
                   ERS_EMPTY, ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE( TrigConf,
                        DBConnectionIssue,
                        TrigConf::PSK2COOLIssue,
                        "DB Connection problem: " << reason,
                        ERS_EMPTY,
                        ((const char *)reason )
                        )

ERS_DECLARE_ISSUE_BASE( TrigConf,
                        FolderAccessIssue,
                        TrigConf::PSK2COOLIssue,
                        "Folder access issue: " << reason,
                        ERS_EMPTY,
                        ((const char *)reason )
                        )

ERS_DECLARE_ISSUE_BASE( TrigConf,
                        DataInsertionIssue,
                        TrigConf::PSK2COOLIssue,
                        "Data insertion issue: " << reason,
                        ERS_EMPTY,
                        ((const char *)reason )
                        )

ERS_DECLARE_ISSUE_BASE( TrigConf,
                        ISPublicationIssue,
                        TrigConf::PSK2COOLIssue,
                        "IS publication issue: " << reason,
                        ERS_EMPTY,
                        ((const char *)reason )
                        )

ERS_DECLARE_ISSUE_BASE( TrigConf,
                        PartitionConfigParsingIssue,
                        TrigConf::PSK2COOLIssue,
                        "Partion configuration parsing issue: " << reason,
                        ERS_EMPTY,
                        ((const char *)reason )
                        )

// namespace daq { namespace rc { class OnlineServices; }}

namespace TrigConf {

   class PrescaleKey2CoolWriter {
   public:

      PrescaleKey2CoolWriter() = delete;

      PrescaleKey2CoolWriter(const std::string & dbalias);

      virtual ~PrescaleKey2CoolWriter();

      bool writeL1Psk2Cool(unsigned int runnr, unsigned int lb, unsigned int l1psk);

      bool writeHLTPsk2Cool(unsigned int runnr, unsigned int lb, unsigned int hltpsk);

      bool writeBothPsk2Cool(unsigned int runnr, unsigned int lb, unsigned int l1psk, unsigned int hltpsk);

      bool writeBgsk2Cool(unsigned int runnr, unsigned int lb, unsigned int bgsk);

      bool writeSmk2Cool(unsigned int runnr, unsigned int smk);

      bool findHLTSetup(std::string& releaseVersion, std::string& asetupArgs) const;

      bool findTriggerDBConnectionAlias(std::string& dbalias) const;

      /**
       * IS related functions
       */

      bool publishSmk2IS(unsigned int smk, const std::string & comment = "");
      bool publishL1Psk2IS(unsigned int l1psk, const std::string & comment = "");
      bool publishHltPsk2IS(unsigned int hltpsk, const std::string & comment = "");
      bool publishBgsk2IS(unsigned int bgsk, const std::string & comment = "");
      bool publishRelease2IS(const std::string & release, const std::string & project = "");

      void getInfoFromIS( unsigned int & smk, unsigned int & l1psk,
                          unsigned int & hltpsk, unsigned int & bgsk,
                          std::string & release );

   protected:

      struct KeysCollection {
         uint32_t l1psk {0};
         uint32_t hltpsk {0};
         uint32_t bgsk {0};         
      };

      // returns the string that is stored in COOL containing dbalias, release, and project
      // Run 2 format was "dbalias,release,project"
      // Run 3 format is "dbalias;release;asetupArgs"
      std::string getConfigSource() const;

      bool dbIsOpen() const;

      void openDb( bool readOnly );
      void closeDb();


      // data members
      const std::string        m_smkFolderName {"/TRIGGER/HLT/HltConfigKeys"};
      const std::string        m_l1pskFolderName {"/TRIGGER/LVL1/Lvl1ConfigKey"};
      const std::string        m_bgskFolderName {"/TRIGGER/LVL1/BunchGroupKey"};
      const std::string        m_hltpskFolderName {"/TRIGGER/HLT/PrescaleKey"};

      // DB connection string: oracle://<server>;schema=<acc_name>;dbname=<db_name>;user=<acc_name>;password=<pwd>
      std::string              m_dbalias { "" };
      cool::IDatabasePtr       m_dbPtr; ///< COOL database pointer

      // Config keys IS locations
      const std::string        m_isLocSMK {"RunParams.TrigConfSmKey"};
      const std::string        m_isLocL1PSK {"RunParams.TrigConfL1PsKey"};
      const std::string        m_isLocBGSK {"RunParams.TrigConfL1BgKey"};
      const std::string        m_isLocHLTPSK {"RunParams.TrigConfHltPsKey"};
      const std::string        m_isLocRel {"RunParams.TrigConfRelease"};

   private:

      bool writeKeys2Cool(const KeysCollection keysColl, unsigned int runnr, unsigned int lb);

      bool publish2IS(ISNamedInfo & isinfo);

   };

}

#endif
